import asyncio
import functools
from itertools import count


def retry(num_retries, exceptions):
    def decorator(func):
        @functools.wraps(func)
        async def decorated(*args, **kwargs):
            for retry_num in count():
                try:
                    return await func(*args, **kwargs)
                except exceptions:
                    if retry_num > num_retries:
                        raise
                    await asyncio.sleep(2 ** retry_num)

        return decorated

    return decorator
