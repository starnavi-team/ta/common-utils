import asyncio

from aiocache import cached as Cached
from asyncio_rlock import RLock


class cached(Cached):
    async def decorator(
            self, f, *args, cache_read=True, cache_write=True, aiocache_wait_for_write=True, **kwargs
    ):
        key = self.get_cache_key(f, args, kwargs)

        if cache_read:
            value = await self.get_from_cache(key)
            if value is not None:
                return value

        vars(self).setdefault('lock', RLock())

        async with self.lock:
            if cache_read:
                value = await self.get_from_cache(key)
                if value is not None:
                    return value

            result = await f(*args, **kwargs)

            if cache_write:
                if aiocache_wait_for_write:
                    await self.set_in_cache(key, result)
                else:
                    asyncio.create_task(self.set_in_cache(key, result))

        return result


call_once = cached
