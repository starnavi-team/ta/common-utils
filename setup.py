# Get the version
from setuptools import find_packages
from setuptools import setup


def get_long_description():
    readme = ''

    with open('README.md', encoding='utf-8') as readme_file:
        readme = readme_file.read()

    return readme


setup(
    name='ta_common',
    version='0.1.0',
    description='Common components for ta project',
    long_description=get_long_description(),
    author='Andrey Kazantcev',
    author_email='heckad@yandex.ru',
    packages=find_packages(exclude=('tests', 'tests.*')),
    include_package_data=True,
    zip_safe=False,
    keywords=['ta_common'],
    install_requires=[
        'asyncio-rlock',
        'aiocache',
        'aionursery',
        'ujson',
        'msgpack'
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: ISC License (ISCL)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ],
    test_suite='tests'
)
