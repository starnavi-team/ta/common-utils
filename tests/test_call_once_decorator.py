import asyncio
from unittest.mock import MagicMock

import pytest

from common.utils.decoraors import call_once


@pytest.mark.asyncio
async def test_concurrent_access():
    call_me = MagicMock()

    @call_once()
    async def func():
        call_me()
        await asyncio.sleep(1)
        return 42

    done, _ = await asyncio.wait([func() for _ in range(10)])

    call_me.assert_called_once()
    assert all([await r == 42 for r in done])
